﻿using Microsoft.Extensions.Logging;
using NeonSource.Infra.Abstractions.Logging;

namespace PocLog
{
    public class Job
    {
        private readonly ILogger<Job> logger;

        public Job(ILogger<Job> logger)
        {
            this.logger = logger;
        }

        public void Start()
        {
            logger
                .Start()
                .WithLogLevel(LogLevel.Information)
                .WithData(new { ClientId = 1020 })
                .WithMessage("Starting Job")
                .Send();
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace PocLog
{
    class Program
    {
        static void Main()
        {
            var provider = IoC.BuildProvider();

            provider.GetRequiredService<Job>().Start();
        }
    }
}

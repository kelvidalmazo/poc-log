﻿using Microsoft.Extensions.DependencyInjection;
using NeonSource.Infra.Logging.Extensions;
using System;

namespace PocLog
{
    public static class IoC
    {
        public static IServiceProvider BuildProvider()
        {
            return new ServiceCollection()
                .AddSingleton<Job>()
                .AddNeonSourceLogWriter(opt => opt.UseElasticCommonSchemaFormatterLogStrategy())
                .BuildServiceProvider();
        }
    }
}
